var http = require('http'),
    url  = require('url');

var server = http.createServer(function(req,res){
   if(req.url == "/"){
        res.writeHead(200,{"Content-type":"text/html;charset=utf-8"});
        res.write("<h1>Pagina Home</h1>");
   }else if(req.url == '/contatos'){
        res.writeHead(200,{"Content-type":"text/html;charset=utf-8"});
        res.write("<h1>Pagina Contatos</h1>");
   }else if(req.url == "/usuarios"){
    res.writeHead(200,{"Content-type":"text/html;charset=utf-8"});
    res.write("<h1>Pagina Usuarios</h1>");
   }else{
        res.writeHead(401,{"Content-type":"text/html;charset=utf-8"});
        res.write("<h1>Pagina Não encontrada!!!!</h1>");
   }

   var resultado = url.parse(req.headers.host);
   res.write("<br>");
   res.write(resultado.href);
   
    res.end();
});

server.listen(3000,function(){
    console.log("Server wakeup!");
});